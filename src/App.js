import logo from './logo.svg';
import './App.css';
import {Card} from './Cards';
import './style.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Color-match</h1>
        <div className="liste">
          <Card value="1" bgColor="blue" />
          <Card value="2" bgColor="yellow"/>
          <Card value="3" bgColor="green"/>
          <Card value="4" bgColor="orange"/>
          <Card value="5" bgColor="pink"/>
          <Card value="6" bgColor="purple"/>
          <Card value="7" bgColor="brown"/>
          <Card value="8" bgColor="grey"/>
          <Card value="9" bgColor="purple"/>
          <Card value="10" bgColor="grey"/>
          <Card value="11" bgColor="brown"/>
          <Card value="12" bgColor="green"/>
          <Card value="13" bgColor="pink"/>
          <Card value="14" bgColor="blue"/>
          <Card value="15" bgColor="yellow"/>
          <Card value="16" bgColor="orange"/>
        </div>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
        </a>
      </header>
      </div>
  );
}

export default App;

import React, { useState } from 'react';

export function Card(props) {
    const [switcher, setSwicth] = useState("active");

    function handlerSwitch(e){
    if(switcher === "active"){
        setSwicth("inactive");
    }
    else {
        setSwicth("active");
    }
}
return(
    <div className={`Card ${switcher}`} style={{background: props.color }} onClick={handlerSwitch}>
            <div class="recto">{props.value}</div>
            <div class="verso"></div>
    
    </div>
);

}